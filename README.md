# peppercarrot/e-reader

An E-Reader of [Pepper&Carrot](https://www.peppercarrot.com/) for Android.

This project is written in Kotlin 1.3.

This project is currently in active development.

# How to build

1. Clone this repository.
1. Import into Android Studio.
1. Download missing packages.
1. Sync project and build/run.

# How to contribute

1. Annoy Me!: Open an issue of something you feel annoying about this app (so that I can be annoyed too).
1. Suggest Me!: Open an issue to suggest a feature that isn't available now.
1. Help Me!: You can translate my app into the language you know, or you can submit a code patch.

# Special Thanks

* David Revoy: for making this awesome webcomic and endorsing this repository.
* Valvin: for providing me with the initial code for language detection.

# License
This project is licensed under GNU GPL 3.0 or higher.
