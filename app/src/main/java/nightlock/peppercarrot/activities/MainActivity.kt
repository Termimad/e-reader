/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.activities

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.TabListener
import nightlock.peppercarrot.fragments.AboutViewPagerFragment
import nightlock.peppercarrot.fragments.ArchiveViewPagerFragment
import nightlock.peppercarrot.fragments.PreferenceFragment
import nightlock.peppercarrot.utils.setAlarm

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, TabListener {
    private val fragmentList = HashMap<Int, Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        launchWelcomeActivity()

        setContentView(R.layout.activity_main)
        setAlarm(this)

        setSupportActionBar(main_toolbar)
        initToolbar(main_toolbar)

        if (savedInstanceState == null) init()
    }

    private fun launchWelcomeActivity() {
        if (!PreferenceManager.getDefaultSharedPreferences(this)
                        .getBoolean(SPLASH_SEEN, false)) {
            /*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notifChannel =
                    NotificationChannel(UPDATE_NOTIF_ID, getString(R.string.name_channel_updates),
                        NotificationManager.IMPORTANCE_DEFAULT)
                notifChannel.description = getString(R.string.description_channel_updates)
                (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
                        .createNotificationChannel(notifChannel)
            }
            */ //TODO: Slated for next release.
            val welcomeIntent = Intent(this, SplashActivity::class.java)
            startActivity(welcomeIntent)
            finish()
        }
    }

    private fun init() {
        //Put Fragments into the list and load ArchiveFragment on FrameLayout
        fragmentList[R.id.nav_archive] = getFragment(R.id.nav_archive)
        fragmentList[R.id.nav_settings] = getFragment(R.id.nav_settings)
        fragmentList[R.id.nav_about] = getFragment(R.id.nav_about)

        swapFragment(getFragment(R.id.nav_archive))
    }

    private fun initToolbar(toolbar: Toolbar) {
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView : NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START))
            drawer_layout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        swapFragment(getFragment(item.itemId))

        // Closes drawer.
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun getFragment(id: Int): Fragment = fragmentList[id] ?: when (id) {
        R.id.nav_archive  -> ArchiveViewPagerFragment()
        R.id.nav_settings -> PreferenceFragment()
        R.id.nav_about    -> AboutViewPagerFragment()
        else              -> {
            Log.wtf("crystal_ball", "Transition to Unknown Fragment Requested!")
            throw RuntimeException("Transition to Unknown Fragment Requested!")
        }
    }

    private fun swapFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, fragment)
                .commit()
    }

    override fun onAttached(): TabLayout {
        return main_tab.apply {
            visibility = View.VISIBLE
        }
    }

    override fun onDetached(): TabLayout {
        return main_tab.apply {
            visibility = View.GONE
            removeAllTabs()
        }
    }

    companion object {
        const val SPLASH_SEEN = "splash_seen"
        //const val UPDATE_NOTIF_ID = "update_notif"
    }
}
