/*
 * Copyright (C) 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.adapters

import android.content.Context
import android.os.Parcelable
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import nightlock.peppercarrot.fragments.ArchiveFragment
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.LanguageDataManager
import nightlock.peppercarrot.utils.jsonToList

/**
 * Adapter for Displaying ArchiveFragment
 * Created by nightlock on 06/02/18.
 */
class ArchiveViewPagerAdapter(fm: FragmentManager, context: Context) : FragmentStatePagerAdapter(fm) {
    private val languages: List<String>
    private val db: LanguageDataManager

    init {
        val manager = PreferenceManager.getDefaultSharedPreferences(context)
        val selectedLangsJson = manager.getString(Language.SELECTED_LANGUAGES, "")
        languages = jsonToList(selectedLangsJson)!!.sorted()
        db = LanguageDataManager(context)
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        try {
            super.restoreState(state, loader)
        } catch (e: NullPointerException) {
        }
    }

    override fun getItem(position: Int): Fragment {
        val language: Language = db.get(languages[position])!!
        return ArchiveFragment.newInstance(language)
    }

    override fun getCount(): Int = languages.size

    override fun getPageTitle(position: Int): CharSequence {
        val language: Language = db.get(languages[position])!!
        return language.localName
    }

}
