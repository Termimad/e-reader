/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import io.github.mthli.sugartask.SugarTask
import kotlinx.android.synthetic.main.fragment_comic_viewer.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.activities.ComicViewerActivity
import nightlock.peppercarrot.utils.jsonToList
import nightlock.peppercarrot.utils.pokeAt

/**
 * Created by nightlock on 5/7/17.
 */

class ComicViewerFragment: Fragment() {
    private val anim by lazy { AnimationUtils.loadAnimation(context, R.anim.blink) }
    private lateinit var imgLink: String

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        if (arguments != null) {
            imgLink = arguments!!.getString(ComicViewerFragment.ARG_LINK)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_comic_viewer, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        comic_image.apply {

            setOnClickListener {
                val activity = activity as ComicViewerActivity
                activity.toggle()
            }
            setOnImageEventListener(object : SubsamplingScaleImageView.DefaultOnImageEventListener() {
                override fun onReady() {
                    super.onReady()
                    setBackgroundColor(Color.WHITE)
                }
            })
        }

        Glide
                .with(this)
                .asBitmap()
                .load("$imgLink.jpg")
                .into(object: SimpleTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        comic_image.setImage(ImageSource.bitmap(resource))
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
                        checkGif("$imgLink.gif")
                    }
                })

        loading_image.startAnimation(anim)
    }

    private fun checkGif(link: String) {
        //TODO: TEMPORARY CODE CREATED FOR GIF COMPATIBILITY. Edit when the API includes the full file URL.
        val options = RequestOptions()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .error(R.drawable.error_placeholder)

        SugarTask
                .with(this)
                .assign {
                    val preference = android.support.v7.preference.PreferenceManager
                            .getDefaultSharedPreferences(context)

                    val availableGifs = preference.getString(ComicViewerFragment.GIF_AVAILABLE, "[]")
                    var available = jsonToList(availableGifs)!!.contains(link)

                    if (!available && pokeAt(link)){
                        preference.edit().apply {
                            val gifs = jsonToList(availableGifs)!!
                            gifs.add(link)
                            putString(ComicViewerFragment.GIF_AVAILABLE,
                                    nightlock.peppercarrot.utils.listToJson(gifs))
                            apply()
                        }
                        available = true
                    }

                    return@assign available
                }.finish { available ->
                    if (!(available as Boolean)){
                        clearAnimation()
                        val errorDrawable = ContextCompat.getDrawable(context!!, R.drawable.error_placeholder)
                        loading_image.setImageDrawable(errorDrawable)
                        return@finish
                    }
                    Glide
                            .with(this)
                            .asGif()
                            .load(link)
                            .apply(options)
                            .listener(object : RequestListener<GifDrawable> {
                                override fun onLoadFailed(e: GlideException?, model: Any?,
                                        target: Target<GifDrawable>?, isFirstResource: Boolean)
                                : Boolean = clearAnimation()

                                override fun onResourceReady(resource: GifDrawable?,
                                        model: Any?, target: Target<GifDrawable>?,
                                        dataSource: DataSource?, isFirstResource: Boolean
                                ): Boolean = clearAnimation()
                            })
                            .into(loading_image)
                }.broken {
                    Log.e("crystal_ball", it.message)
                    clearAnimation()
                    val errorDrawable = ContextCompat.getDrawable(context!!, R.drawable.error_placeholder)
                    loading_image.setImageDrawable(errorDrawable)
                }.execute()
    }

    private fun clearAnimation(): Boolean{
        loading_image.clearAnimation()
        loading_image.alpha = 1f
        return false
    }

    companion object {
        private const val ARG_LINK = "link"
        private const val GIF_AVAILABLE = "tested_gif"

        fun newInstance(link: String): ComicViewerFragment {
            val fragment = ComicViewerFragment()
            val args = Bundle().apply {
                putString(ARG_LINK, link)
            }
            fragment.arguments = args
            return fragment
        }
    }
}
