/*
 * Copyright (C) 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.fragments

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_about_app.*
import kotlinx.android.synthetic.main.card_about_author.*
import nightlock.peppercarrot.R

/**
 * Created by nightlock on 18/03/18.
 */
class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_about, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        app_version.text = getVersionCode()
        view_source_container.setOnClickListener { openUrl(SOURCE_LINK) }
        view_privacy_policy_container.setOnClickListener { openUrl(resources.getString(R.string.privacy_policy_link)) }
        icon_email_nightlock.setOnClickListener { openEmail(EMAIL_ADDRESS_NIGHTLOCK) }
    }

    private fun getVersionCode(): String {
        try {
            return context!!.packageManager.getPackageInfo(context!!.packageName, 0)
                    .versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return context!!.getString(R.string.version_unknown)
    }

    private fun openUrl(url: String) {
        val i = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(i)
    }

    private fun openEmail(email: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:$email")
            putExtra(Intent.EXTRA_EMAIL, email)
            putExtra(Intent.EXTRA_SUBJECT, "Reader for Pepper&Carrot")
        }
        startActivity(Intent.createChooser(intent, "E-Mail"))
    }

    companion object {
        const val SOURCE_LINK = "https://framagit.org/peppercarrot/e-reader"
        const val EMAIL_ADDRESS_NIGHTLOCK = "imsesaok@tuta.io"
    }
}
