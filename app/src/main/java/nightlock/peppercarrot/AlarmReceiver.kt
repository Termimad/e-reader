/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.preference.PreferenceManager
import android.util.Log
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.LanguageDataManager
import nightlock.peppercarrot.utils.isConnected
import nightlock.peppercarrot.utils.setAlarm
import javax.net.ssl.SSLHandshakeException

/**
 * Broadcast Receiver for updating comic list.
 * Created by graphene on 24/08/17.
 */

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null) return //If context is not available the whole code will not work.

        if (intent?.action == "android.intent.action.BOOT_COMPLETED") {
            setAlarm(context)
            return
        }

        if (!isConnected(context)) return

        val preference = PreferenceManager.getDefaultSharedPreferences(context)
        if (!preference.getBoolean("download_on_mobile_network", false)) {
            val networkInfo = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            if (networkInfo.activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE)
                return
        }

        /*
        val notification = NotificationCompat
                .Builder(context, MainActivity.UPDATE_NOTIF_ID)
                .setSmallIcon(R.drawable.ic_launcher_web)
                .setContentTitle(context.getString(R.string.new_updates))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        */ //TODO: Slated for next release.
        Thread {
            context.let {
                try {
                    val updated = ArchiveDataManager.updateArchive(it)
                    LanguageDataManager.updateLanguage(it)

                    /*
                    val manager =
                        android.preference.PreferenceManager.getDefaultSharedPreferences(it)
                    val selectedLangsJson = manager.getString(Language.SELECTED_LANGUAGES, "")
                    val selectedLanguages = jsonToList(selectedLangsJson)!!
                    val languageDb = LanguageDataManager(context)

                    for (episode in updated) {
                        for (language in episode.supported_languages) {
                            if (selectedLanguages.contains(language)) {
                                NotificationManagerCompat
                                        .from(it)
                                        .notify(NOTIF_ID, notification
                                                .setContentText(it.getString(R.string.update_entry,
                                                        episode.index + 1,
                                                        languageDb.get(language)!!.localName))
                                                .build())
                            }
                        }
                    }
                    */ //TODO: Slated for next release.
                } catch (e: SSLHandshakeException){
                    Log.e("crystal_ball", "SSLHandshakeException; Spoofed Connection?")
                }
            }
        }.start()

    }

    companion object {
        const val NOTIF_ID = 42
    }
}
