/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.viewholders

import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.drawable.Drawable
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import nightlock.peppercarrot.R
import nightlock.peppercarrot.activities.ComicViewerActivity
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.getCoverImageUrl
import nightlock.peppercarrot.utils.getEmptyCoverImageUrl

/**
 * ViewHolder for containing the episode cover.
 * Created by Jihoon Kim on 5/2/17.
 */

class EpisodeCoverViewHolder private constructor(v: View, val language: Language) :
        BaseViewHolder<Episode>(v) {
    private val imageView : ImageView = v.findViewById(R.id.episode_image)
    private val context = imageView.context!!
    lateinit var episode: Episode
    init {
        imageView.setOnClickListener {
            if (language.pncCode in episode.supported_languages) {
                val intent = Intent(context, ComicViewerActivity::class.java).apply {
                    putExtra(Episode.EPISODE, episode)
                    putExtra(Language.LANGUAGE, language)
                }
                context.startActivity(intent)
            } else {
                Snackbar
                        .make(v, R.string.selected_episode_not_available, Snackbar.LENGTH_LONG)
                        .show()
            }
        }
    }

    private val anim = AnimationUtils.loadAnimation(context, R.anim.blink)
    val greyMatrix = ColorMatrix().apply {
        setSaturation(0f)
    }

    override fun onBind(data: Episode) {
        this.episode = data

        val link =
                if (language.pncCode in data.supported_languages)
                    getCoverImageUrl(data, language)
                else
                    getEmptyCoverImageUrl(data)

        imageView.startAnimation(anim)

        val imageOption = RequestOptions()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.error_placeholder)

        Glide
                .with(context)
                .load(link)
                .thumbnail(.3f)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?,
                                              target: Target<Drawable>?, isFirstResource: Boolean): Boolean {

                        imageView.apply {
                            clearAnimation()
                            clearColorFilter()
                            alpha = 1f
                        }
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?,
                                                 target: Target<Drawable>?, dataSource: DataSource?,
                                                 isFirstResource: Boolean): Boolean {
                        imageView.apply {
                            clearAnimation()
                            if (language.pncCode in data.supported_languages) {
                                clearColorFilter()
                                alpha = 1f
                            } else {
                                colorFilter = ColorMatrixColorFilter(greyMatrix)
                                alpha = 0.5f
                            }
                        }
                        return false
                    }
                })
                .apply(imageOption)
                .into(imageView)
    }

    companion object {
        fun newInstance(viewGroup: ViewGroup, language: Language) : EpisodeCoverViewHolder {
            val view = LayoutInflater
                    .from(viewGroup.context)
                    .inflate(R.layout.card_episode, viewGroup, false)
            return EpisodeCoverViewHolder(view, language)
        }
    }
}
