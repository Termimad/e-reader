package nightlock.peppercarrot.viewholders

import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import nightlock.peppercarrot.R
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.jsonToList
import nightlock.peppercarrot.utils.listToJson

/**
 * ViewHolder for Individual Language Selection.
 * Created by nightlock on 21/12/17.
 */
class LanguageSelectionViewHolder private constructor(v: View) :
        BaseViewHolder<Pair<Language, Boolean>>(v) {
    private val checkBox: CheckBox = v.findViewById(R.id.language_checkbox)
    private val cardView: CardView = v.findViewById(R.id.language_selection_card)
    private val languageName: TextView = v.findViewById(R.id.language_name_text)
    private val recommendedText: TextView = v.findViewById(R.id.recommended_language_text)
    private val preference: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(v.context)
    private lateinit var language: Language

    init {
        cardView.setOnClickListener { checkBox.performClick() }
        checkBox.setOnClickListener {
            val languages: MutableList<String>
            val selectedLangsJson = preference.getString(Language.SELECTED_LANGUAGES, "[]")
            if (selectedLangsJson != "[]") {
                languages = jsonToList(selectedLangsJson)!!

                if (!checkBox.isChecked) languages.remove(language.pncCode)
                else languages.add(language.pncCode)
            } else {
                languages = ArrayList()
                languages.add(language.pncCode)
            }

            preference
                    .edit()
                    .putString(Language.SELECTED_LANGUAGES, listToJson(languages))
                    .apply()
        }
    }

    override fun onBind(data: Pair<Language, Boolean>) {
        this.language = data.first
        val recommended = data.second
        val visibility = if (recommended) View.VISIBLE else View.GONE
        recommendedText.visibility = visibility
        languageName.text = language.localName
        val selectedLangsJson = preference.getString(Language.SELECTED_LANGUAGES, "")
        checkBox.isChecked = selectedLangsJson.contains(language.pncCode)
    }

    companion object {
        fun newInstance(viewGroup: ViewGroup): LanguageSelectionViewHolder {
            val view = LayoutInflater
                    .from(viewGroup.context)
                    .inflate(R.layout.fragment_languageselection, viewGroup, false)
            return LanguageSelectionViewHolder(view)
        }
    }
}
