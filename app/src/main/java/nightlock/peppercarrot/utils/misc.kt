/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.SystemClock
import com.google.gson.GsonBuilder
import nightlock.peppercarrot.AlarmReceiver

/**
 * Miscellaneous utility functions.
 * Created by Jihoon Kim on 21/07/17.
 */

const val ROOT_URL = "https://www.peppercarrot.com/0_sources/"

fun getCoverImageUrl(episode: Episode, language: Language) = ROOT_URL +
        "${episode.name}/low-res/${language.pncCode}_Pepper-and-Carrot_by-David-Revoy_" +
        "E${String.format("%1$02d", episode.index + 1)}.jpg"

fun getEmptyCoverImageUrl(episode: Episode) = ROOT_URL +
        "${episode.name}/low-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_" +
        "E${String.format("%1$02d", episode.index + 1)}.jpg"

fun getBaseComicImageUrl(episode: Episode, language: Language, index: Int) = ROOT_URL +
        "${episode.name}/low-res/${language.pncCode}_Pepper-and-Carrot_by-David-Revoy_" +
        "E${String.format("%1$02d", episode.index + 1)}P${String.format("%1$02d", index)}"

fun listToJson(list: List<String>): String = GsonBuilder()
        .create()
        .toJson(list)

fun jsonToList(string: String): MutableList<String>? = GsonBuilder()
        .create()
        .fromJson(string, Array<String>::class.java)
        .toMutableList()


fun setAlarm(context: Context) {
    val manager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val intent = Intent(context, AlarmReceiver::class.java)
    val pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)

    manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
            SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_FIFTEEN_MINUTES,
            AlarmManager.INTERVAL_HALF_DAY, pending)
}

fun isConnected(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return cm.activeNetworkInfo != null
}
