Reader for Pepper&Carrot ("the APP") *does not* store or transmit personal information.

The APP connects to third-party site over the internet; [Sources](https://www.peppercarrot.com/0_sources/) page of the [Official Pepper&Carrot Website](https://www.peppercarrot.com/).
The APP connects to this (and *only* in this directory) sites *only* to serve the core purpose of the APP.
No personal data are transmitted in the process, and the APP does not handle cookies.
Refer to the Privacy Policy of each site for more information.

This document will be updated promptly whenever a change has happened to how the APP handles your personal data.

This document was last updated on Nov. 8, 2018.